#!/bin/bash
intern=LVDS1
extern=HDMI3

if xrandr | grep "$extern disconnected"; then
    xrandr --output "$extern" --off --output "$intern" --auto
else
    xrandr --output "$intern" --primary --auto --output "$extern" --left-of "$intern" --auto
fi
