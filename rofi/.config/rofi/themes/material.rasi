/*
 * ROFI color theme
 *
 * Based on Base16 Material Color Scheme (https://github.com/ntpeters/base16-materialtheme-scheme)
 *
 * User: Tomaszal
 * Copyright: Tomas Zaluckij
 */

configuration {
	display-drun:    "activate";
	display-run:     "execute";
	display-window:  "window";
	show-icons:      true;
	sidebar-mode:    true;
}

* {
	base00: #263238;
	base01: #2E3C43;
	base02: #314549;
	base03: #546E7A;
	base04: #B2CCD6;
	base05: #EEFFFF;
	base06: #EEFFFF;
	base07: #FFFFFF;
	base08: #F07178;
	base09: #F78C6C;
	base0A: #FFCB6B;
	base0B: #C3E88D;
	base0C: #89DDFF;
	base0D: #82AAFF;
	base0E: #C792EA;
	base0F: #FF5370;

	/*base0D: #00BCD4;*/

	spacing: 0;
	background-color: transparent;

	font: "Roboto Mono 13";
        sidebar-mode: true;
}

window {
	transparency: "real";
	fullscreen: true;
	background-color: #263238CC; /*base00 + CC (80% opacity)*/
        padding: 300px 500px;
}

mainbox {
	children: [inputbar, message, listview, sidebar];
	spacing: 30px;
	/*margin: 20%;*/
	padding: 30px 0;
}

inputbar {
	padding: 0;
	children: [prompt, textbox-prompt-colon, entry, case-indicator];
}

prompt {
	text-color: @base0D;
}

textbox-prompt-colon {
	expand: false;
	str: ":";
	margin: 0 1ch 0 0;
	text-color: @base0D;
}

entry {
	text-color: @base07;
}

case-indicator {
	text-color: @base0F;
}

sidebar {
        background-color: transparent;
}

button, textbox {
	background-color: transparent;
	text-color: @base07;
	padding: 5px;
}

button selected {
	background-color: @base0D;
}

listview {
	margin: 0;
}

element {
	padding: 5px;
	highlight: bold underline;
}

element normal {
	background-color: transparent;
}

element selected {
	background-color: @base0D;
}

element alternate {
	/*background-color: @base03;*/
}

element normal normal, element selected normal, element alternate normal {
	text-color: @base07;
}

element normal urgent, element selected urgent, element alternate urgent {
	text-color: @base0F;
}

element normal active, element selected active, element alternate active {
	text-color: @base0B;
}
