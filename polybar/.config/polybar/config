;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/jaagr/polybar
;
;   The README contains alot of information
;
;==========================================================

[colors]
background = ${xrdb:color1}
transparent = #00000000
background-alt = ${xrdb:color2}
foreground = ${xrdb:color7}
foreground-alt = ${xrdb:color3}
primary = #ffb52a
secondary = #e60053
alert = #bd2c40

[bar/bar1]
monitor = ${env:MONITOR:VGA1}
width = 100%
height = 20
;offset-x = 1%
;offset-y = 1%
radius = 0.0
fixed-center = true

background = ${colors.transparent}
foreground = ${colors.foreground}

line-size = 5
line-color = #f00

border-size = 5
border-color = #00000000

font-0 = fixed:pixelsize=10;2
font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
font-2 = siji:pixelsize=10;2
font-3 = Font Awesome 5 Free:pixelsize=10;2
font-4 = Font Awesome 5 Free Solid:pixelsize=10;2
font-5 = Font Awesome 5 Brands:pixelsize=10;2
font-6 = ProFontIIx Nerd Font:fontformat=truetype:size=16:antialias=true;3

modules-left = leftbeg space powermenu sepleft date sepleft i3 sepleft screenicon leftend
modules-center =
modules-right = rightend space xbacklight space alsa sepright xkeyboard sepright wlan space eth space expressvpn sepright battery space rightbeg

tray-position = center
tray-padding = 2
;tray-transparent = true
;tray-background = ${colors.background}

;scroll-up = i3wm-wsnext
;scroll-down = i3wm-wsprev

cursor-click = pointer
cursor-scroll = ns-resize

[module/polynews]
type = custom/script
exec = /home/lars/.polyscripts/polynews/polynews-theguardian.py
interval = 30
format-prefix = " "

[module/expressvpn]
type = custom/script
exec = /home/lars/.expressstatus.sh
interval = 1
format-prefix = " "
format-background = ${colors.background}

[module/xwindow]
type = internal/xwindow
label = %title:0:30:...%

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "

label-layout = %layout%

label-indicator-padding = 2
label-indicator-margin = 1
format-background = ${colors.background}

[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted-prefix = " "
label-mounted =  %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/i3]
type = internal/i3
format = <label-state> <label-mode>

index-sort = true
wrapping-scroll = true
pin-workspaces = false
strip-wsnumbers = false

background = ${colors.background}

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${self.background}
label-focused-padding = 2

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-foreground = ${colors.foreground-alt}
label-unfocused-background = ${self.background}
label-unfocused-padding = 2

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.background}
label-visible-padding = 2

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

; Separator in between workspaces
label-separator = "  "
label-separator-padding = 2
label-separator-background = ${self.background}



[module/mpd]
type = internal/mpd
format-online = <label-song>  <icon-prev> <icon-stop> <toggle> <icon-next>

icon-prev =  
icon-stop =  
icon-play =  
icon-pause =  
icon-next =  

label-song-maxlen = 25
label-song-ellipsis = true

[module/xbacklight]
type = internal/xbacklight

#format = <label> <bar>
#label =  

format-prefix = " " 
label= %percentage%%



bar-width = 10
bar-indicator = |
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = ─
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = ─
bar-empty-font = 2
bar-empty-foreground = ${colors.foreground-alt}
format-background = ${colors.background}

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
label = %percentage:2%%
format-background = ${colors.background}

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
label = %percentage_used%%
format-background = ${colors.background}


[module/wlan]
type = internal/network
interface = wlan
interval = 3.0

format-connected-prefix = " "
format-connected = <label-connected>
label-connected = %{A1:connman-gtk:} %{A}

format-disconnected-prefix = " "
format-disconnected = <label-disconnected>
label-disconnected = %{A1:connman-gtk:} %{A}

format-connected-background = ${colors.background}
format-disconnected-background = ${colors.background}

[module/eth]
type = internal/network
interface = eth
interval = 3.0

format-connected-prefix = " "
label-connected = %{A1:connman-gtk:} %{A}

format-disconnected-prefix = " "
label-disconnected = %{A1:connman-gtk:} %{A}

format-connected-background = ${colors.background}
format-disconnected-background = ${colors.background}

[module/date]
type = internal/date
interval = 5

date =
date-alt = " %Y-%m-%d"

time = %H:%M
time-alt = %H:%M:%S

format-prefix = " "

label = %date% %time%
format-background = ${colors.background}

[module/alsa]
type = internal/pulseaudio
master-mixer = Master

#format-volume = <label-volume> <bar-volume>
#label-volume =   %percentage%%
#label-volume-foreground = ${root.foreground}

#format-volume-prefix
#
#label-muted = 🔇 muted
#label-muted-foreground = #666
#
#bar-volume-width = 10
#bar-volume-foreground-0 = #55aa55
#bar-volume-foreground-1 = #55aa55
#bar-volume-foreground-2 = #55aa55
#bar-volume-foreground-3 = #55aa55
#bar-volume-foreground-4 = #55aa55
#bar-volume-foreground-5 = #f5a70a
#bar-volume-foreground-6 = #ff5555
#bar-volume-gradient = false
#bar-volume-indicator = |
#bar-volume-indicator-font = 2
#bar-volume-fill = ─
#bar-volume-fill-font = 2
#bar-volume-empty = ─
#bar-volume-empty-font = 2
#bar-volume-empty-foreground = ${colors.foreground-alt}

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

format-volume = <ramp-volume> <label-volume>
label-muted = " muted"
label-muted-foreground = ${colors.foreground-alt}
label-muted-underline = #f0f0f0


mapping = false
format = <label>
format-muted-background = ${colors.background}
format-volume-background = ${colors.background}

[module/battery]
type = internal/battery
battery = BAT0
adapter = AC
full-at = 98

format-charging = <animation-charging> <label-charging>
format-discharging = <animation-discharging> <label-discharging>
format-full-prefix = "  "

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-foreground = ${colors.foreground-alt}

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-framerate = 750

animation-discharging-0 =  
animation-discharging-1 =  
animation-discharging-2 =  
animation-discharging-framerate = 750
format-charging-background = ${colors.background}
format-discharging-background = ${colors.background}
format-full-background = ${colors.background}

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format = <ramp> <label>
format-warn = <ramp> <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-foreground = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 

[module/powermenu]
type = custom/menu

expand-right = true

format-spacing = 1

label-open = 
label-open-foreground = ${colors.foreground}
label-close =  cancel
label-close-foreground = ${colors.secondary}
label-separator = |
label-separator-foreground = ${colors.foreground-alt}

menu-0-0 = reboot
menu-0-0-exec = menu-open-1
menu-0-1 = power off
menu-0-1-exec = menu-open-2

menu-1-0 = cancel
menu-1-0-exec = menu-open-0
menu-1-1 = reboot
menu-1-1-exec = reboot

menu-2-0 = power off
menu-2-0-exec = poweroff
menu-2-1 = cancel
menu-2-1-exec = menu-open-0
format-background = ${colors.background}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over

[global/wm]
margin-top = 5
margin-bottom = 5

[module/leftend]
type = custom/text
content = 
content-foreground = ${colors.background}
content-background = ${colors.transparent}

[module/rightend]
type = custom/text
content = 
content-foreground = ${colors.background}
content-background = ${colors.transparent}

[module/rightbeg]
type = custom/text
content = 
content-foreground = ${colors.background}
content-background = ${colors.transparent}

[module/leftbeg]
type = custom/text
content = 
content-foreground = ${colors.background}
content-background = ${colors.transparent}

[module/leftend-alt]
type = custom/text
content = 
content-foreground = ${colors.background-alt}
content-background = ${colors.background}

[module/rightend-alt]
type = custom/text
content = 
content-foreground = ${colors.background-alt}
content-background = ${colors.background}

[module/rightbeg-alt]
type = custom/text
content = 
content-foreground = ${colors.background-alt}
content-background = ${colors.background}

[module/leftbeg-alt]
type = custom/text
content = 
content-foreground = ${colors.background-alt}
content-background = ${colors.background}

[module/space]
type = custom/text
content = "   "
content-background = ${colors.background}

[module/sepright]
type = custom/text
content = "      "
content-background = ${colors.background}

[module/sepleft]
type = custom/text
content = "      "
content-background = ${colors.background}

[module/screenicon]
type = custom/text
content = "    "
content-background = ${colors.background}
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev

