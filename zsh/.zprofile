 export PATH=~/.npm-global/bin:$PATH
 export SUDO_EDITOR=nvim
 export ANDROID_HOME="/home/lars/.android-sdk"
 export PATH=/home/lars/.flutter-sdk/bin:$PATH
 export VISUAL=nvim
 export EDITOR="$VISUAL"
 alias ls='ls --color=auto'
 alias pac="sudo pacman"
 alias pacrm="~/.pacrm.sh"
 alias scrne="mons -e left --primary VGA1"
 alias scrnr="mons --primary LVDS1"
 alias copy="xclip -selection clipboard"
 alias vim=nvim

# start X at boot
if [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
