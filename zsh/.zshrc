export LANG="en_US.UTF-8"

export ZSH="/home/lars/.oh-my-zsh"
ZSH_THEME="powerlevel9k/powerlevel9k"

#Powerlevel9k config
#POWERLEVEL9K_COLOR_SCHEME='light'
POWERLEVEL9K_MODE='awesome'
POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(dir dir_writable vcs)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status time)
DISABLE_AUTO_TITLE="true"
POWERLEVEL9K_SHORTEN_DIR_LENGTH=4
export DEFAULT_USER="$USER"
POWERLEVEL9K_OS_ICON_BACKGROUND="white"
POWERLEVEL9K_OS_ICON_FOREGROUND="blue"
POWERLEVEL9K_DIR_HOME_FOREGROUND="white"
POWERLEVEL9K_DIR_HOME_BACKGROUND="green"
POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND="white"
POWERLEVEL9K_DIR_DEFAULT_FOREGROUND="white"
DISABLE_AUTO_UPDATE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
plugins=(
  git
)

#alias
alias ls='ls --color=auto'
alias pac="sudo pacman"
alias pacrm="~/.pacrm.sh"
alias scrne="mons -e left --primary VGA1"
alias scrnr="mons --primary LVDS1"
alias copy="xclip -selection clipboard"
alias home="cd ~ && clear"


source $ZSH/oh-my-zsh.sh

cat /home/lars/.cache/wal/sequences
export GPG_TTY=/dev/pts/1
gpg-connect-agent updatestartuptty /bye > /dev/null
unset SSH_AGENT_PID
export SSH_AUTH_SOCK=/run/user/1000/gnupg/S.gpg-agent.ssh
